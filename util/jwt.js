const jwt = require("jsonwebtoken");
const SECRET_KEY = "secret-key";

const generateToken = (user) => {
  return jwt.sign({ userId: user.id, email: user.email }, SECRET_KEY, {
    expiresIn: "1h",
  });
};

const decodeToken = (token) => {
  try {
    const decoded = { data: jwt.verify(token, SECRET_KEY), message: "" };
    return decoded;
  } catch (error) {
    return { data: null, message: error.message };
  }
};

module.exports = { generateToken, decodeToken };
