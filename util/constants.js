const QuestionSetConstants = {
  status: "In Progress",
  points: 30,
  total: 0,
  hard: 0,
  medium: 0,
};

module.exports = { QuestionSetConstants };
