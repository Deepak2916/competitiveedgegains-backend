const prpareUserResponseData = (type, success, message, user, jwt) => {
  if (type === "login") {
    return {
      success,
      message,
      user,
      jwt,
    };
  } else {
    return {
      success,
      message,
    };
  }
};

module.exports = { prpareUserResponseData };
