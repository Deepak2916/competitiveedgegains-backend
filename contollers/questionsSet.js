const QuestionSet = require("../models/questionsModel");
const { QuestionSetConstants } = require("../util/constants");
const createQuestionSet = async (args, { user, message }) => {
  try {
    if (!user) {
      return {
        success: false,
        message: `UnAutherised (${message})`,
      };
    }

    const newQuestionSet = new QuestionSet({
      ...QuestionSetConstants,
      createdUser: {
        email: user.email,
        name: user.username,
      },
      createdUserId: user._id,
      subject: args.subject,
      exam: args.exam,
    });
    await newQuestionSet.save();

    return {
      success: true,
      message: "String",
      data: [newQuestionSet],
    };
  } catch (error) {
    console.log(error);
    return {
      success: false,
      message: error.message,
      data: null,
    };
  }
};

const getQuestionSetsByCreatedUserId = async (
  { subject, exam },
  { user, message }
) => {
  try {
    if (!user) {
      return {
        success: false,
        message: `UnAutherised (${message})`,
      };
    }
    let questionSets = [];

    switch (user.role) {
      case "master":
        questionSets = await QuestionSet.find({
          createdUserId: user._id,
          subject: subject,
          exam: exam,
        });
        break;
      case "super admin":

      case "admin":
        questionSets = await QuestionSet.find({
          subject: subject,
          exam: exam,
        });
        break;
      case "student":
        questionSets = await QuestionSet.find({
          subject: subject,
          exam: exam,
          status: "accepted",
        });
        break;
      default:
        break;
    }
    // questionSets = await QuestionSet.find({
    //   subject: subject,
    //   exam: exam,
    // });
    // if (user.role === "master") {
    //   questionSets = await QuestionSet.find({
    //     createdUserId: user._id,
    //     subject: subject,
    //     exam: exam,
    //   });
    // }
    console.log(questionSets);

    return {
      success: true,
      message: "questionSets fetched successfully",
      data: questionSets,
    };
  } catch (error) {
    return {
      success: false,
      message: error.message,
    };
  }
};

const deleteQuestionSetsById = async ({ id }, { user, message }) => {
  try {
    if (!user) {
      return {
        success: false,
        message: `UnAutherised (${message})`,
      };
    }

    const deleteSet = await QuestionSet.deleteOne({ _id: id });

    return {
      success: true,
      message: "questionSet deleted",
    };
  } catch (error) {
    return {
      success: false,
      message: error.message,
    };
  }
};

const updateQuestionSetById = async (
  { id, update, isEdit, status },
  { user, message }
) => {
  try {
    if (!user) {
      return {
        success: false,
        message: `Unauthorized (${message})`,
        data: [],
      };
    }
    if (user.role === "admin" || user.role === "super admin") {
      const updatedQuestionsSet = await QuestionSet.findOneAndUpdate(
        { _id: id },
        {
          ...update,
        }
      );

      return {
        success: true,
        message: "Updated successfully",
        data: [updatedQuestionsSet],
      };
    }
    const updatedQuestionsSet = await QuestionSet.findOne({ _id: id });
    if (!updatedQuestionsSet) {
      return {
        success: false,
        message: "QuestionSet not found",
        data: [],
      };
    }
    if (isEdit) {
      updatedQuestionsSet.questions = updatedQuestionsSet.questions.map(
        (data) => {
          if (data.slNo === update.slNo) {
            return {
              ...data,
              question: update.question,
              options: update.options,
            };
          }
          return data;
        }
      );
      // console.log("updatedQuestionsSet", updatedQuestionsSet);
    } else {
      updatedQuestionsSet[update.difficulty] =
        updatedQuestionsSet[update.difficulty] + 1;
      updatedQuestionsSet.total += 1;
      updatedQuestionsSet.questions.push({
        ...update,
        slNo: updatedQuestionsSet.total,
      });
    }
    await updatedQuestionsSet.save();

    return {
      success: true,
      message: "Updated successfully",
      data: [updatedQuestionsSet],
    };
  } catch (error) {
    console.log(error);
    return {
      success: false,
      message: error.message,
      data: [],
    };
  }
};

module.exports = {
  createQuestionSet,
  getQuestionSetsByCreatedUserId,
  deleteQuestionSetsById,
  updateQuestionSetById,
};
