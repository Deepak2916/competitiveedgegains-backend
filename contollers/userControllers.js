const bcrypt = require("bcrypt");

const User = require("../models/userModel");
const { prpareUserResponseData } = require("../util/responses");
const { generateToken } = require("../util/jwt");

const saltRounds = 10;

const createUser = async (data) => {
  try {
    const superAdmin = process.env.SUPER_ADMIN;
    const user = await User.findOne({ email: data.email }).exec();
    if (user) {
      // prpareResponseData = (type, sucess, message)
      return prpareUserResponseData(
        "signup",
        false,
        `${user.role} already exists.`
      );
    }
    if (data.email === superAdmin) {
      data.role = "super admin";
    }

    const passwordHash = bcrypt.hashSync(data.password, saltRounds);
    const newUser = new User({
      ...data,
      password: passwordHash,
    });
    await newUser.save();

    return prpareUserResponseData(
      "signup",
      true,
      `new ${newUser.role} created`
    );
  } catch (error) {
    return prpareUserResponseData("signup", true, error.message);
  }
};

const login = async (email, password) => {
  try {
    const user = await User.findOne({ email }).exec();

    if (user) {
      const isPasswordCorrect = await bcrypt.compare(password, user.password);
      // prpareResponseData = (type, sucess, message, user, jwt)

      return isPasswordCorrect
        ? prpareUserResponseData(
            "login",
            true,
            "Login success",
            user,
            generateToken(user)
          )
        : prpareUserResponseData(
            "login",
            false,
            "Password not correct",
            null,
            ""
          );
    }

    return prpareUserResponseData("login", false, "User not found", null, "");
  } catch (error) {
    return prpareUserResponseData("login", false, error.message, null, "");
  }
};

const getLoginUserData = ({ user, message }) => {
  if (user) {
    return prpareUserResponseData("login", true, message, user, "");
  } else {
    return prpareUserResponseData("login", false, message, null, "");
  }
};
const getUsersByRole = async ({ role }, { user, message }) => {
  if (user && (user.role === "admin" || user.role === "super admin")) {
    const users = await User.find({ role });

    return {
      success: true,
      message: `${role} users fetched successfully`,
      users,
    };
  }

  return {
    success: false,
    message: "UnAutherised",
    users: null,
  };
};
module.exports = { login, createUser, getLoginUserData, getUsersByRole };
