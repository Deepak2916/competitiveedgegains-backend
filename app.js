const express = require("express");
const server = require("./graphql/schema");
const connectDB = require("./util/db");
const cors = require("cors");
const app = express();
const dotenv = require("dotenv");
dotenv.config();
app.use(express.json());

connectDB();

app.use(cors());

async function startApolloServer() {
  await server.start();
  server.applyMiddleware({ app, path: "/my-graphql-endpoint" });

  const PORT = process.env.PORT || 4000;
  app.listen(PORT, () => {
    console.log(
      `Server is running at http://localhost:${PORT}${server.graphqlPath}`
    );
  });
}

startApolloServer();
