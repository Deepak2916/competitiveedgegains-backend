// schema.js

const { ApolloServer } = require("apollo-server-express");
const { merge } = require("lodash");

const userTypes = require("./typesDefs/userTypes");

const userResolvers = require("./resolvers/userResolvers");
const { decodeToken } = require("../util/jwt");
const User = require("../models/userModel");
const questionsTypes = require("./typesDefs/questionsTypes");
const questionsResolvers = require("./resolvers/questionsResolvers");

const typeDefs = [userTypes, questionsTypes];
const resolvers = merge(userResolvers, questionsResolvers);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const token = req.headers.authorization || "";
    if (token) {
      const res = decodeToken(token);
      if (res.data) {
        const user = await User.findOne({ email: res.data.email }).exec();
        return { user, message: "user login success" };
      }
      return { user: null, message: res.message };
    }
    return { user: null, message: "required jwt" };
  },
  cache: "bounded",
});

module.exports = server;
