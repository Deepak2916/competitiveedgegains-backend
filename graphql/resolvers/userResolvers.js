const {
  login,
  createUser,
  getLoginUserData,
  getUsersByRole,
} = require("../../contollers/userControllers");

const userResolvers = {
  Query: {
    getLoginUserData: (_, { username, password }, context) => {
      return getLoginUserData(context);
    },
    getUsersByRole: (_, args, context) => {
      return getUsersByRole(args, context);
    },
  },
  Mutation: {
    createUser: (_, args, context, info) => createUser(args),
    login: async (_, { email, password }) => {
      const res = await login(email, password);

      return res;
    },
  },
};

module.exports = userResolvers;
