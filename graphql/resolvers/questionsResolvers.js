const {
  createQuestionSet,
  getQuestionSetsByCreatedUserId,
  deleteQuestionSetsById,
  updateQuestionSetById,
} = require("../../contollers/questionsSet");

const questionsResolvers = {
  Query: {
    getQuestionSetsByCreatedUserId: (_, args, context) => {
      return getQuestionSetsByCreatedUserId(args, context);
    },
  },
  Mutation: {
    createQuestionSet: (_, { subject, exam }, context) => {
      return createQuestionSet({ subject, exam }, context);
    },
    deleteQuestionSetsById: (_, args, context) => {
      return deleteQuestionSetsById(args, context);
    },
    updateQuestionSetById: (_, args, context) => {
      return updateQuestionSetById(args, context);
      // {
      //   success: true,
      //   message: "Question set updated successfully",
      //   data: [
      //     {
      //       _id: "12345",
      //       createdUserId: "67890",
      //       createdUser: {
      //         name: "John Doe",
      //         email: "john.doe@example.com",
      //       },
      //       subject: "Math",
      //       exam: "Final Exam",
      //       submittedAt: "2024-02-09T12:34:56Z",
      //       acceptedAt: "2024-02-10T10:00:00Z",
      //       acceptedUserId: "54321",
      //       acceptedUser: {
      //         name: "Jane Doe",
      //         email: "jane.doe@example.com",
      //       },
      //       status: "accepted",
      //       points: 80,
      //       questions: [
      //         {
      //           slNo: 1,
      //           difficulty: "easy",
      //           question: "What is 2 + 2?",
      //           options: [
      //             { value: "3" },
      //             { value: "4" },
      //             { value: "5" },
      //             { value: "6" },
      //           ],
      //         },
      //         {
      //           slNo: 2,
      //           difficulty: "medium",
      //           question: "What is the capital of France?",
      //           options: [
      //             { value: "Berlin" },
      //             { value: "Madrid" },
      //             { value: "Paris" },
      //             { value: "Rome" },
      //           ],
      //         },
      //       ],
      //       total: 2,
      //       hard: 0,
      //       easy: 1,
      //       medium: 1,
      //     },
      //   ],
      // };
    },
  },
};

module.exports = questionsResolvers;
