// userTypes.js
const { gql } = require("apollo-server-express");

const userTypes = gql`
  type User {
    _id: ID
    username: String!
    email: String!
    password: String
    phoneNumber: String
    totalQuestionsAdded: Int
    totalQuestionsAttempted: Int
    totalQuestionsSolved: Int
    totalPointsGained: Int
    points: Int
    isApprovedByAdmin: Boolean
    totalRejectedQuestions: Int
    role: String
    profileImg: String
  }

  type Course {
    id: String!
    name: String!
  }

  type Query {
    getLoginUserData: AuthResult
    getUsersByRole(role: String): getUseResult
  }

  type getUseResult {
    success: Boolean!
    message: String
    users: [User]
  }

  input CourseInput {
    id: String!
    name: String!
  }

  type AuthResult {
    success: Boolean!
    message: String
    user: User
    jwt: String
  }

  type Mutation {
    login(email: String!, password: String!): AuthResult
    createUser(
      username: String!
      email: String!
      password: String
      phoneNumber: String
      role: String
    ): CreateUserResult
  }

  type CreateUserResult {
    success: Boolean!
    message: String
  }
`;

module.exports = userTypes;
