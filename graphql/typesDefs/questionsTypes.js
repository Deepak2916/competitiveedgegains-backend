const { gql } = require("apollo-server-express");

const questionsTypes = gql`
  type User {
    name: String
    email: String
  }

  type Question {
    slNo: Int
    difficulty: String
    question: String
    options: [String]
    correctOption: String
  }

  type QuestionSet {
    _id: ID
    createdUserId: ID
    createdUser: User
    subject: String
    exam: String
    submittedAt: String
    acceptedAt: String
    acceptedUserId: ID
    acceptedUser: User
    status: String
    points: Int
    questions: [Question]
    total: Int
    hard: Int
    easy: Int
    medium: Int
  }

  type Response {
    success: Boolean
    message: String
    data: [QuestionSet]
  }

  type Mutation {
    createQuestionSet(subject: String!, exam: String!): Response

    deleteQuestionSetsById(id: String): Response

    updateQuestionSetById(
      id: String
      update: UpdateInput
      isEdit: Boolean
    ): Response
  }

  input UpdateInput {
    slNo: Int
    question: String
    options: [String]
    difficulty: String
    correctOption: String
    status: String
  }

  type Query {
    getQuestionSetsByCreatedUserId(subject: String!, exam: String!): Response
  }
`;

module.exports = questionsTypes;
