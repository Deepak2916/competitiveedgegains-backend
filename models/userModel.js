const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  username: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true, unique: true },
  phoneNumber: { type: String, required: true, unique: true },
  points: { type: Number },
  role: { type: String, default: "user" },
  isApprovedByAdmin: { type: Boolean, default: false },
  profileImg: { type: String },
  totalQuestionsAdded: { type: Number },
  totalQuestionsAttempted: { type: Number },
  totalQuestionsSolved: { type: Number },
  totalPointsGained: { type: Number },
  totalRejectedQuestions: { type: Number },
});

const User = mongoose.model("User", userSchema);

module.exports = User;
