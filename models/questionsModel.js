const mongoose = require("mongoose");

const userSchema = {
  name: { type: String },
  email: { type: String },
};

const questionSetsSchema = new mongoose.Schema(
  {
    createdUserId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    createdUser: userSchema,

    total: { type: Number, default: 0 },
    hard: { type: Number, default: 0 },
    easy: { type: Number, default: 0 },
    medium: { type: Number, default: 0 },
    subject: { type: String, required: true },
    exam: { type: String, required: true },
    submittedAt: { type: Date },
    acceptedAt: { type: Date },
    acceptedUserId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    acceptedUser: userSchema,
    status: {
      type: String,
      enum: ["In Progress", "Submitted", "Accepted", "Rejected"],
    },
    points: { type: Number, required: true },
    questions: [
      {
        slNo: { type: Number },
        difficulty: {
          type: String,
          enum: ["hard", "medium", "easy"],
        },
        question: { type: String },
        options: [{ type: String }],
        correctOption: { type: String },
      },
    ],
  },
  { timestamps: true }
);

const QuestionSet = mongoose.model("QuestionSet", questionSetsSchema);

module.exports = QuestionSet;
